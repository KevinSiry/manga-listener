from xml.etree import ElementTree

import requests

from service.models import MangaUser, Manga
from service.tools.data_access_layer import DataAccessLayer
from service.tools.feed_mangas import FeedMangas
import re


class MangaAllController:

    @staticmethod
    def feed_db_manga():
        FeedMangas().get_content_directory()

    def update_mangas(self):
        try:
            mangas = MangaUser.objects.values('manga').distinct()
            for i in mangas:
                manga = Manga.objects.get(id=i.get('manga'))
                self.update_manga(manga)
            DataAccessLayer().add_last_update()
        except Exception as error:
            print(error)

    def update_manga(self, manga):
        try:
            r = requests.get(f'http://fanfox.net/rss/{manga.tag}.xml')
            manga = Manga.objects.get(tag=manga.tag)
            if not r.text:
                return False
            root = ElementTree.fromstring(r.text)
            last_chapter = root.find('channel').find('item').find('title').text
            last_chapter = re.search("(?<=Ch.)(\\d+.\\d+)", last_chapter).group(0)
            if manga.last_chapter != float(last_chapter):
                DataAccessLayer().update_manga(manga, {'last_chapter': float(last_chapter)})
                mangas = MangaUser.objects.filter(manga=manga.id)
                for m in mangas:
                    data = {'url': FeedMangas.get_next_chapter_url(manga.tag, m.last_read)}
                    DataAccessLayer().update_mangas_for_user(m, data)

        except Exception as error:
            print(error)

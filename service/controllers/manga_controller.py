import re
from xml.etree import ElementTree

import requests

from common.exceptions import NotFoundException
from service.models import Manga, MangaUser
from service.tools.data_access_layer import DataAccessLayer
from service.tools.feed_mangas import FeedMangas
from service.tools.serializers.manga_serializer import MangaSerializer
from service.tools.user_manager import UserManager


class MangaController:

    @staticmethod
    def link_manga_user(request):
        data = request.data
        user = UserManager().get_user_by_name(
            request.query_params.get('username')).id
        try:
            manga = Manga.objects.get(tag=data.get('tag')).id
            return DataAccessLayer().create_manga_user({'user': user, 'manga': manga})
        except Exception as error:
            print(error)
            raise NotFoundException('This manga does not exist')

    @staticmethod
    def get_mangas_for_user(request):
        try:
            user = UserManager().get_user_by_name(
                request.query_params.get('username')).id
            return DataAccessLayer().retrieve_mangas_for_user(user)
        except Exception as error:
            print(error)

    @staticmethod
    def update_mangas_for_user(username, tag, data):
        try:
            user = UserManager().get_user_by_name(username).id
            manga = Manga.objects.get(tag=tag).id
            last_read = data.get('last_read')
            favorite = data.get('favorite')
            new_data = {}
            if 'last_read' in data:
                new_data = {
                    'last_read': last_read,
                    'url': FeedMangas.get_next_chapter_url(tag, last_read)
                }
            elif 'favorite' in data:
                new_data['favorite'] = favorite
            return DataAccessLayer().update_manga_for_user(user, manga, new_data)
        except Exception as error:
            print(error)

    @staticmethod
    def get_mangas(value):
        instance = Manga.objects.filter(name__icontains=value)[:20]
        return MangaSerializer(instance, many=True).data

    @staticmethod
    def create_manga(data, xml):
        last_chapter = xml.find('channel').find('item').find('title').text
        last_chapter = re.search("(?<=Ch.).*$", last_chapter)
        return DataAccessLayer().create_manga(
            {
                'name': xml.find('channel').find('title').text,
                'tag': data['tag'],
                'last_chapter': last_chapter.group(0)
            })

    @staticmethod
    def delete_manga_for_user(username, tag):
        try:
            user = UserManager().get_user_by_name(username).id
            manga = Manga.objects.get(tag=tag).id
            MangaUser.objects.get(user=user, manga=manga).delete()
        except Exception as error:
            print(error)

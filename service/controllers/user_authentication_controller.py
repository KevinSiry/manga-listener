from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token

from common.exceptions import InvalidCredentialsException
from service.tools.serializers.user_auth_serializer import UserAuthSerializer


class UserAuthenticationController:

    @staticmethod
    def login_user(username, password):
        user = authenticate(username=username, password=password)
        if not user:
            raise InvalidCredentialsException('Invalid credentials')
        Token.objects.get_or_create(user=user)

        return UserAuthSerializer(user).data

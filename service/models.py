from django.contrib.auth.models import User
from django.db import models


class Manga(models.Model):
    name = models.TextField()
    tag = models.TextField(default=None)
    last_chapter = models.FloatField(default=None, null=True)


class MangaUser(models.Model):
    manga = models.ForeignKey(Manga, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    last_read = models.FloatField(default=None, null=True)
    url = models.TextField(default=None, null=True)
    favorite = models.BooleanField(default=False)

    class Meta:
        unique_together = ('manga', 'user',)


class MangasLastUpdate(models.Model):
    last_update = models.DateTimeField()

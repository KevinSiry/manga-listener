from datetime import timedelta

from django.conf import settings
from django.utils import timezone


class TokenExpirationHandler:

    @staticmethod
    def expires_in(token):
        time_elapsed = timezone.now() - token.created
        return timedelta(seconds=settings.TOKEN_EXPIRATION_TIME) - time_elapsed

    def is_token_expired(self, token):
        return self.expires_in(token) < timedelta(seconds=0)

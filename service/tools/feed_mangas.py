import re
from xml.etree import ElementTree

from lxml import html
import requests

from service.models import Manga
from service.tools.data_access_layer import DataAccessLayer


class FeedMangas:

    def get_content_directory(self):
        r = requests.get(f'http://fanfox.net/directory/')
        tree = html.fromstring(r.content)
        max_page = tree.xpath('//div[@class="pager-list-left"]/a/text()')[-2]

        for page in range(int(max_page)):
            print(page)
            r = requests.get(f'http://fanfox.net/directory/{page + 1}.html')
            tree = html.fromstring(r.content)
            mangas = tree.xpath('//div[@class="manga-list-1"]/ul/li')
            for manga in mangas:
                tag = manga.find('a').attrib['href'].split('/')[-2]
                name = manga.find('a').attrib['title']
                last_chapter = manga.findall('p')[1].find('a').text
                last_chapter = re.search("(?<=Ch.).*$", last_chapter)
                if tag and name and last_chapter:
                    last_chapter = last_chapter.group(0)

                    self.create_or_update_manga({'name': name, 'tag': tag, 'last_chapter': last_chapter})

    def create_or_update_manga(self, data):
        try:
            instance = Manga.objects.get(tag=data['tag'])
            if data.get('last_chapter'):
                DataAccessLayer().update_manga(instance, {'last_chapter': data.get('last_chapter')})
        except Exception:
            if data.get('name') and data.get('tag') and data.get('last_chapter'):
                DataAccessLayer().create_manga(data)

    @staticmethod
    def get_next_chapter_url(tag, last_chapter):
        try:
            if '.0' in str(last_chapter):
                last_chapter = int(last_chapter)
            if '.' in str(last_chapter):
                last_chapter = str(last_chapter).zfill(5)
            else:
                last_chapter = str(last_chapter).zfill(3)
            r = requests.get(f'http://fanfox.net/rss/{tag}.xml')
            if not r.text:
                return False
            root = ElementTree.fromstring(r.text)
            last_url = None
            for elem in root.iter('item'):
                chap = re.search("(?<=Ch.)(\\d+.\\d+)",
                                 elem.find('title').text).group(0)
                if last_chapter == chap:
                    return last_url
                last_url = elem.find('link').text
        except Exception as error:
            print(error)

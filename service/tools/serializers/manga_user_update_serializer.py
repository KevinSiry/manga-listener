from rest_framework import serializers

from service.models import MangaUser


class MangaUserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = MangaUser
        fields = (
            'last_read',
            'url',
            'favorite'
        )

    def update(self, instance, validated_date):
        if 'last_read' in validated_date:
            instance.last_read = validated_date.get('last_read')
        if 'url' in validated_date:
            instance.url = validated_date.get('url')
        if 'favorite' in validated_date:
            instance.favorite = validated_date.get('favorite')

        instance.save()
        return instance

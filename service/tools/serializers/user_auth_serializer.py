from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from service.tools.serializers.user_serializer import UserSerializer


class UserAuthSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'user',
            'token',
            'is_staff',
        )

    @staticmethod
    def get_user(user):
        return UserSerializer(user).data

    @staticmethod
    def get_token(user):
        token = Token.objects.get(user=user)
        return token.key

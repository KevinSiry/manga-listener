from rest_framework import serializers

from service.models import Manga


class MangaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Manga
        fields = (
            'id',
            'name',
            'tag',
            'last_chapter',
        )

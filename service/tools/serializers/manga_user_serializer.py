from rest_framework import serializers

from service.models import MangaUser


class MangaUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MangaUser
        fields = (
            'manga',
            'user',
            'last_read',
            'favorite'
        )

from rest_framework import serializers

from service.models import MangaUser
from service.tools.serializers.manga_serializer import MangaSerializer


class MangaUserRetrieveSerializer(serializers.ModelSerializer):
    manga = MangaSerializer()

    class Meta:
        model = MangaUser
        fields = (
            'manga',
            'last_read',
            'url',
            'favorite'
        )

from rest_framework import serializers

from service.models import Manga


class MangaUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Manga
        fields = (
            'last_chapter',
        )

    def update(self, instance, validated_date):
        if 'last_chapter' in validated_date:
            instance.last_chapter = validated_date.get('last_chapter')

        instance.save()
        return instance

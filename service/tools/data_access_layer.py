from django.utils import timezone

from service.models import Manga, MangaUser, MangasLastUpdate
from service.tools.serializers.manga_serializer import MangaSerializer
from service.tools.serializers.manga_update_serializer import MangaUpdateSerializer
from service.tools.serializers.manga_user_retrieve_serializer import MangaUserRetrieveSerializer
from service.tools.serializers.manga_user_serializer import MangaUserSerializer
from service.tools.serializers.manga_user_update_serializer import MangaUserUpdateSerializer


class DataAccessLayer:

    @staticmethod
    def save(serializer):
        if serializer.is_valid():
            instance = serializer.save()
            return MangaSerializer(instance).data
        else:
            raise Exception(serializer.errors)

    def create_manga(self, manga):
        serializer = MangaSerializer(data=manga)
        return self.save(serializer)

    @staticmethod
    def retrieve_manga(instance):
        return MangaSerializer(instance=instance).data

    def update_manga(self, instance, data, partial_value=True):
        serializer = MangaUpdateSerializer(instance, data=data, partial=partial_value)
        self.save(serializer)

    @staticmethod
    def create_manga_user(manga_user):
        serializer = MangaUserSerializer(data=manga_user)
        if serializer.is_valid():
            instance = serializer.save()
            return MangaUserRetrieveSerializer(instance).data
        else:
            raise Exception(serializer.errors)

    @staticmethod
    def retrieve_mangas_for_user(user_id):
        return MangaUserRetrieveSerializer(instance=MangaUser.objects.filter(user=user_id).order_by('id'),
                                           many=True).data

    @staticmethod
    def update_manga_for_user(user_id, manga_id, data, partial_value=True):
        instance = MangaUser.objects.get(user=user_id, manga=manga_id)
        serializer = MangaUserUpdateSerializer(instance, data=data, partial=partial_value)
        if serializer.is_valid():
            serializer.save()
        else:
            raise Exception(serializer.errors)

    @staticmethod
    def update_mangas_for_user(instance, data, partial_value=True):
        serializer = MangaUserUpdateSerializer(instance, data=data, partial=partial_value)
        if serializer.is_valid():
            serializer.save()
        else:
            raise Exception(serializer.errors)

    @staticmethod
    def add_last_update():
        instance = MangasLastUpdate(last_update=timezone.now())
        instance.save()

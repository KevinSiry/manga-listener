from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class UserManager:

    @staticmethod
    def get_user_by_token(token):
        key = str(token.strip('Bearer ')).strip()
        user = Token.objects.get(key=key).user
        return User.objects.get(username=user)

    @staticmethod
    def get_user_by_name(name):
        return User.objects.get(username=name)

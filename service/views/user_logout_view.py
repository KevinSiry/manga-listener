from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from common.exceptions import InvalidCredentialsException, InvalidParametersException


class UserLogoutView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        try:
            token = request.META.get('HTTP_AUTHORIZATION')
            key = str(token.strip('Bearer ')).strip()
            Token.objects.get(key=key).delete()
            return Response(status=status.HTTP_200_OK)
        except InvalidParametersException as errors:
            return Response(
                errors.to_dict(), status=status.HTTP_400_BAD_REQUEST)
        except InvalidCredentialsException as errors:
            return Response(
                errors.to_dict(), status=status.HTTP_403_FORBIDDEN)
        except Exception as error:
            return Response(
                str(error),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

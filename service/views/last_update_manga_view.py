from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.models import MangasLastUpdate


class LastUpdateMangaView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        try:
            last_update_manga = MangasLastUpdate.objects.latest('last_update')
            return Response({'last_update': last_update_manga.last_update},
                            status=status.HTTP_200_OK)
        except Exception:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

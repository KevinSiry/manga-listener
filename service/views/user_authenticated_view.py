from rest_framework import generics, status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.tools.token_expiration_handler import TokenExpirationHandler


class UserAuthenticatedView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        """Return the token and time left to its expiry (in seconds)"""
        key = str(request.META.get('HTTP_AUTHORIZATION')).strip('Bearer ').strip()
        token = Token.objects.get(key=key)

        return Response(
            {'token': key,
             'expires_in': TokenExpirationHandler().expires_in(token)},
            status=status.HTTP_200_OK)

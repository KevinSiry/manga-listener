from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.controllers.manga_controller import MangaController


class MangaUserUpdateView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def patch(request, **kwargs):
        manga = MangaController().update_mangas_for_user(
            request.query_params.get('username'),
            kwargs['tag'],
            request.data)
        return Response(manga, status=status.HTTP_200_OK)

    @staticmethod
    def delete(request, **kwargs):
        MangaController().delete_manga_for_user(
            request.query_params.get('username'),
            kwargs['tag'])
        return Response(status=status.HTTP_200_OK)

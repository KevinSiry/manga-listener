from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.controllers.manga_controller import MangaController


class MangaUserRetrieveView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        manga = MangaController().get_mangas_for_user(request)
        return Response(manga, status=status.HTTP_200_OK)

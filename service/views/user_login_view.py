from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from common.exceptions import InvalidCredentialsException, InvalidParametersException
from service.controllers.user_authentication_controller import UserAuthenticationController


class UserLoginView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def post(request):
        try:
            data = UserAuthenticationController().login_user(
                request.data.get('username'),
                request.data.get('password'))
            return Response(data, status=status.HTTP_200_OK)
        except InvalidParametersException as errors:
            return Response(
                errors.to_dict(), status=status.HTTP_400_BAD_REQUEST)
        except InvalidCredentialsException as errors:
            return Response(
                errors.to_dict(), status=status.HTTP_403_FORBIDDEN)
        except Exception:
            return Response(
                {'An unexpected error occurred'},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

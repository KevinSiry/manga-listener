from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.controllers.manga_all_controller import MangaAllController
from service.tools.user_manager import UserManager


class MangasUpdateView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        try:
            user = UserManager().get_user_by_name(
                request.query_params.get('username'))
            if user.is_staff:
                MangaAllController().update_mangas()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response('You cannot perform this action',
                                status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            print(error)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

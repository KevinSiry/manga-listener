from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from common.exceptions import NotFoundException
from service.controllers.manga_all_controller import MangaAllController
from service.controllers.manga_controller import MangaController
from service.tools.user_manager import UserManager


class MangaCreateView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def post(request):
        try:
            manga = MangaController().link_manga_user(request)
            return Response(manga, status=status.HTTP_201_CREATED)
        except NotFoundException as error:
            return Response(error.to_dict(),
                            status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request):
        try:
            user = UserManager().get_user_by_name(
                request.query_params.get('username'))
            if user.is_staff:
                MangaAllController().feed_db_manga()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response('You cannot perform this action', status=status.HTTP_400_BAD_REQUEST)
        except Exception:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.tools.user_manager import UserManager


class UserView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        user = UserManager().get_user_by_name(
            request.query_params.get('username'))
        if user.is_staff:
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

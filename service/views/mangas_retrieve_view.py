from rest_framework import generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from service.controllers.manga_controller import MangaController


class MangasRetrieveView(generics.GenericAPIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        mangas = MangaController().get_mangas(request.query_params.get('search'))
        return Response(mangas, status=status.HTTP_200_OK)

from django.contrib import admin

# Register your models here.
from service.models import Manga, MangaUser

admin.site.register(Manga)
admin.site.register(MangaUser)

"""manga_listener URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from service.views import manga_create_view, manga_user_retrieve_view, user_register_view, mangas_retrieve_view
from service.views.last_update_manga_view import LastUpdateMangaView
from service.views.manga_user_update_view import MangaUserUpdateView
from service.views.mangas_update_view import MangasUpdateView
from service.views.user_authenticated_view import UserAuthenticatedView
from service.views.user_login_view import UserLoginView
from service.views.user_logout_view import UserLogoutView
from service.views.user_view import UserView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('manga', manga_create_view.MangaCreateView.as_view()),
    path('mangas-user', manga_user_retrieve_view.MangaUserRetrieveView.as_view()),
    path('mangas-user/<str:tag>', MangaUserUpdateView.as_view()),
    path('mangas', mangas_retrieve_view.MangasRetrieveView.as_view()),
    path('mangas-update', MangasUpdateView.as_view()),
    path('user', UserView.as_view()),
    # register
    path('register', user_register_view.UserRegisterView.as_view()),
    # authentication_services
    path('user/login/', UserLoginView.as_view(),
         name='user-auth-login'),
    path('user/logout/', UserLogoutView.as_view(),
         name='user-auth-logout'),
    path('user/authenticated/',
         UserAuthenticatedView.as_view(),
         name='user-auth-authenticated'),
    path('last_update', LastUpdateMangaView.as_view(), name='last-update')
]

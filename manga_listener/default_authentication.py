from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed


# DEFAULT_AUTHENTICATION_CLASS
from service.tools.token_expiration_handler import TokenExpirationHandler


class ExpiringTokenAuthentication(TokenAuthentication):

    def authenticate_credentials(self, key):
        try:
            token = Token.objects.get(key=key)
        except Token.DoesNotExist:
            raise AuthenticationFailed("Invalid Token")

        if not token.user.is_active:
            raise AuthenticationFailed("User is not active")

        if TokenExpirationHandler().is_token_expired(token):
            token.delete()
            raise AuthenticationFailed("The Token is expired")

        # an authentication function must return this tuple of values
        return token.user, token

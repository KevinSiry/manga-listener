from django.utils.deprecation import MiddlewareMixin
from rest_framework import status


class UserMiddleware(MiddlewareMixin):

    @staticmethod
    def process_response(request, response):
        if response.status_code == status.HTTP_401_UNAUTHORIZED:
            response.status_code = status.HTTP_403_FORBIDDEN
        return response
